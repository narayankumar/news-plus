<?php
/**
 * @file
 * news_plus.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function news_plus_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function news_plus_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function news_plus_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_news
  $nodequeues['ad_block_news'] = array(
    'name' => 'ad_block_news',
    'title' => 'Ad block - News',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function news_plus_node_info() {
  $items = array(
    'news' => array(
      'name' => t('Ginger News'),
      'base' => 'node_content',
      'description' => t('Use <em>News</em> for Indian or international news reports, whether created/copied over by editor or submitted by a writer'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Remember to SAVE after you Preview.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
