build2014092701
- label of ct changed to 'ginger news'

build2014092101
- views changes
  - comments field in teasers view made into a link

build2014091802
- change exposed filter label to 'news categories'
- replaced type in teasers view with news categories taxonomy term

build2014091801
- made title 'news' in teasers view
- gave it class=black, h4 and strong

build2014091602
- remove sharethis news label
- provide add comment box

7.x-1.0-dev1
- initial commit created on 12 sep 2014
