<?php
/**
 * @file
 * news_plus.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function news_plus_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'news_items_blocks';
  $context->description = 'Blocks displaying News on node pages';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'news' => 'news',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'news_teasers_page' => 'news_teasers_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-news_teasers_page-block_2' => array(
          'module' => 'views',
          'delta' => 'news_teasers_page-block_2',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-recent_news_comments-block_1' => array(
          'module' => 'views',
          'delta' => 'recent_news_comments-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks displaying News on node pages');
  $export['news_items_blocks'] = $context;

  return $export;
}
