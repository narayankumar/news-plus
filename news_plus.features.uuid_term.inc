<?php
/**
 * @file
 * news_plus.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function news_plus_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Business',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '06c69e95-f6cc-4dee-8edd-c062e2811b50',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Science & Technology',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0e59568d-4198-4e89-a7b3-e4e4207d014e',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Events',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '161e42f1-c68a-444a-a642-29e0afbfb70a',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Pet Care',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '40b01568-af6a-4f21-9f94-a411e091f4c9',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Inspirational',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7b24fc3e-37ae-4d6f-a944-66754788884c',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Animal Adventure',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b3e2c7f6-7c5f-42db-bcbc-4466494dc2f1',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Obituary',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'ce76c9cb-da80-4c8c-a840-509daff7c906',
    'vocabulary_machine_name' => 'news_categories',
    'metatags' => array(),
  );
  return $terms;
}
